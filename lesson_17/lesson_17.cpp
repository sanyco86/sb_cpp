﻿#include <iostream>
using namespace std;

class Vector
{
public:
    Vector()
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        cout << x << ", " << y << ", " << z << endl;
    }

    double Module() {
        return sqrt(x * x + y * y + z * z);
    }

private:
    double x = 0;
    double y = 0;
    double z = 0;
};

int main()
{
    Vector v(10, 20, 30);
    v.Show();
    cout << "Vector module: " << v.Module() << endl;
}
