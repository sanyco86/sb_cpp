﻿#include <iostream>
#include <time.h>

static int CurrentDay()
{
    // Получаем текущий день месяца
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    return buf.tm_mday;
}

int main()
{
    const int N = 5; // Задаем размерность массива

    // Создаем двумерный массив размерности N × N и заполняем его
    int array[N][N];
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            array[i][j] = i + j;
        }
    }

    // Выводим массив в консоль
    std::cout << "Array iteration:\n";
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout <<" " << array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    // Вычисляем индекс строки для суммирования элементов
    int rowIndex = CurrentDay() % N;

    // Вычисляем сумму элементов в этой строке
    int rowSum = 0;
    for (int j = 0; j < N; ++j) {
        rowSum += array[rowIndex][j];
    }

    // Выводим сумму элементов в строке с индексом rowIndex
    std::cout << "\nSum elements in row by index: " << rowIndex << ": " << rowSum << std::endl;

    return 0;
}
