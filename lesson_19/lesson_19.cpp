﻿#include <iostream>
#include <vector>

class Animal {
public:
    virtual void Voice() const {
        std::cout << "Some generic animal sound" << std::endl;
    }
};

class Dog : public Animal {
public:
    void Voice() const override {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal {
public:
    void Voice() const override {
        std::cout << "Meow!" << std::endl;
    }
};

class Bird : public Animal {
public:
    void Voice() const override {
        std::cout << "Tweet!" << std::endl;
    }
};

int main() {
    // Создание массива указателей типа Animal
    std::vector<Animal*> animals;

    // Добавление объектов созданных классов в массив
    animals.push_back(new Dog());
    animals.push_back(new Cat());
    animals.push_back(new Bird());

    // Вызов метода Voice() для каждого объекта в массиве
    for (const auto& animal : animals) {
        animal->Voice();
    }

    // Освобождение памяти, выделенной для объектов
    for (const auto& animal : animals) {
        delete animal;
    }

    return 0;
}
