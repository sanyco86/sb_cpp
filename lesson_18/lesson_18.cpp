﻿#include <iostream>
#include <algorithm>
using namespace std;

class Player
{
public:
    Player()
    {}
    Player(const string& _name, int _score) : name(_name), score(_score)
    {}

    string getName() const
    {
        return name;
    }

    int getScore() const
    {
        return score;
    }

private:
    string name;
    int score;
};

// Функция для сравнения двух игроков по убыванию очков
static bool comparePlayers(const Player& p1, const Player& p2)
{
    return p1.getScore() > p2.getScore();
}

static void printScoreTable(int numPlayers, Player* players)
{
    cout << "\nSorted list of players:\n";
    for (int i = 0; i < numPlayers; ++i)
    {
        cout << players[i].getName() << ": " << players[i].getScore() << " points\n";
    }
}

static void setPlayers(Player* players, int numPlayers)
{
    for (int i = 0; i < numPlayers; ++i)
    {
        string name;
        int score;
        cout << "Enter the player's name " << i + 1 << ": ";
        cin >> name;
        cout << "Enter the number of points for the player " << name << ": ";
        cin >> score;
        players[i] = Player(name, score);
    }
}

int main()
{
    int numPlayers;
    cout << "Enter the number of players: ";
    cin >> numPlayers;

    // Создание динамического массива игроков
    Player* players = new Player[numPlayers];

    // Получение от пользователя имен и очков игроков и сохранение в массиве
    setPlayers(players, numPlayers);

    // Сортировка массива игроков по убыванию количества очков
    sort(players, players + numPlayers, comparePlayers);

    // Вывод имен и очков игроков в отсортированном виде
    printScoreTable(numPlayers, players);

    // Освобождение выделенной памяти для массива игроков
    delete[] players;

    return 0;
}
