﻿#include <iostream>
#include "Helpers.h"
using namespace std;

int main()
{
    int a = 5;
    int b = 3;
    int result = SquareOfSum(a, b);
    cout << "Квадрат суммы чисел " << a << " и " << b << " равен " << result << endl;
    return 0;
}
