#pragma once
int SquareOfSum(int num1, int num2) {
    int sum = num1 + num2;
    int square = sum * sum;
    return square;
}
