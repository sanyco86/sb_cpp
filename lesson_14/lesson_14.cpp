﻿#include <iostream>
#include <string>

int main()
{
    // Создание и инициализация строки
    std::string MyString = "Hello, world!";

    // Вывод самой строки
    std::cout << "Строка: " << MyString << std::endl;

    // Вывод длины строки
    std::cout << "Длина строки: " << MyString.length() << std::endl;

    // Вывод первого символа строки
    std::cout << "Первый символ строки: " << MyString.front() << std::endl;

    // Вывод последнего символа строки
    std::cout << "Последний символ строки: " << MyString.back() << std::endl;

    return 0;
}
