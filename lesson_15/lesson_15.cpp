﻿#include <iostream>
using namespace std;

// Функция для вывода чётных чисел от 0 до N
void printEvenNumbers(int N) {
    cout << "Even numbers from 0 to " << N << ": ";

    for (int i = 0; i <= N; i += 2) {
        cout << i << " ";
    }
    cout << endl;
}
// Функция для вывода нечётных чисел от 0 до N
void printOddNumbers(int N) {
    cout << "Odd numbers from 0 to " << N << ": ";

    for (int i = 1; i <= N; i += 2) {
        cout << i << " ";
    }
    cout << endl;
}

// Функция для вывода чётных или нечётных чисел от 0 до N
void printNumbers(int N, bool isOdd) {
    cout << (isOdd ? "Odd" : "Even") << " numbers from 0 to " << N << " (with single function) : ";

    int start = 1 * isOdd;

    for (int i = start; i <= N; i += 2) {
        cout << i << " ";
    }
    cout << endl;
}

int main() {
    const int N = 21; // Задаём верхний предел для вывода чисел

    // Вывод чётных чисел от 0 до N
    printEvenNumbers(N);

    // Вывод нечётных чисел от 0 до N
    printOddNumbers(N);

    // Вывод чётных чисел от 0 до N с помощью функции
    printNumbers(N, false);

    // Вывод нечётных чисел от 0 до N с помощью функции
    printNumbers(N, true);

    return 0;
}
